package com.ahmed.ateeq

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val addCntBtn = findViewById<Button>(R.id.addCntBtn)
        val resetBtn = findViewById<Button>(R.id.resetCntBtn)
        val counterTxtView = findViewById<TextView>(R.id.counterTxtView)

        var counterCount = counterTxtView.text.toString().toInt()

        addCntBtn.setOnClickListener {

            counterCount += 1

            counterTxtView.text = counterCount.toString()

            if (counterCount > 10){

                AteeqAppNotification.notify(this, R.string.ateeq_app_notification_title_template.toString(), R.string.ateeq_app_notification_placeholder_text_template )
            }
        }

        resetBtn.setOnClickListener {

            counterCount = 0
            counterTxtView.text = counterCount.toString()
        }

    }
}
